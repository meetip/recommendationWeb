
from py2neo import Graph
from py2neo import Node
from py2neo import Relationship 
import os
import operator 
from nltk.stem.wordnet import WordNetLemmatizer
import math
import time
from datetime import timedelta


class RecommendationSearch():
	"""docstring for ClassName"""
	#graph = 0
	def setdb(self):
		dblink = os.environ.get('GRAPHENEDB_URL', 'http://localhost:7474') 
		username = "neo4j"
		password = "1q2w3e4r"
		graph = Graph(dblink + '/db/data/', username=username, password=password)
		#graph = Graph("http://neo4j:1q2w3e4r@localhost:7474/db/data/")
		return graph

	def __init__(self, arg):
		super(RecommendationSearch, self).__init__()
		self.arg = arg
		self.graph= self.setdb()
	def setdata(self,total, rc_item, keyphrase_item,totalN):
		self.total = total
		self.rc_item = rc_item
		self.keyphrase_item = keyphrase_item
		self.totalN = totalN
 
	def getSearchRank(self,mode,q):	
		if mode ==1:
			result = self.graph.data("MATCH (Document)-[r:TF_BASELINE]->(Keyword) "
			"WHERE Keyword.word =~{keys} "
			"RETURN Document.title as title, Document.id as id, Keyword.word as matched, r.weight as weight "
			"ORDER BY r.weight DESC", {"keys": ".*" + q + ".*"})
		elif mode ==2:
			result = self.graph.data("MATCH (Document)-[r:TF_LEMMA]->(Keyword) "
			"WHERE Keyword.word =~{keys} "
			"RETURN Document.title as title,  Document.id as id, Keyword.word as matched,  r.weight as weight "
			"ORDER BY r.weight DESC", {"keys": ".*" + q + ".*"})
		elif mode ==3:
			result = self.graph.data("MATCH (Document)-[r:RAKE]->(Keyword) "
			"WHERE Keyword.word =~{keys} "
			"RETURN Document.title as title, ,Document.id as id, Keyword.word as matched,  r.weight as weight "
			"ORDER BY r.weight DESC", {"keys": ".*" + q + ".*"})
		else:
			result = self.graph.data("MATCH (Document)-[r:TEXTRANK]->(Keyword) "
			"WHERE Keyword.word =~{keys} "
			"RETURN Document.title as title, Document.id as id, Keyword.word as matched,  r.weight as weight "
			"ORDER BY r.weight DESC", {"keys": ".*" + q + ".*"})
		 
		return result 
			
	def computeRankResult(self, resultlist):				 
		resultset = set([(d['title'],d['id']) for d in resultlist])
		qresult = {} 
		for item in resultset:
			score = 0
			itemlist = [d for d in resultlist if d['title'] ==item[0]]
			for i in itemlist:
				score += i['weight']
			qresult[item] = score
		 
		 
		sorted_result = sorted(qresult.items(), key=lambda x: x[1], reverse=True)
		result = [r[0] for r in sorted_result]
		print(result)
		
		 
		return result	
	
	def getDocumentdetail(self,title):
		result = self.graph.data("MATCH (d:Document) WHERE d.title={keys} "
			"RETURN d.title as title, d.id as id, d.abstract as abstract", keys=title)
		res = []
		res.append(result[0]['title'])
		res.append( result[0]['id'])
		res.append( result[0]['abstract'])
		return res	
	
	def searchwithKeyword(self,q): 
	
		q = q.lower()
		n_gram_query = q.split()
		lmtzr = WordNetLemmatizer()
		TFbaselineResult=[]
		TFLemmaResult=[]
		RAKEResult=[]
		TextRankResult=[]
		for item in n_gram_query:
			q = lmtzr.lemmatize(item) 
			TFbaselineResult+=self.getSearchRank(1, q)
			TFLemmaResult+=self.getSearchRank(2, q)
			RAKEResult+=self.getSearchRank(3, q)
			TextRankResult+=self.getSearchRank(4, q)
			
		TFbaselineResult = self.computeRankResult(TFbaselineResult)
		TFLemmaResult = self.computeRankResult(TFLemmaResult)
		RAKEResult = self.computeRankResult(RAKEResult)
		TextRankResult	= self.computeRankResult(TextRankResult) 
		print("TFBaseline")
		for row in TFbaselineResult:
			print((row))
		print("TFLemma") 
		for row in TFLemmaResult:
			print(row)
		print("RAKE")
		for row in RAKEResult:
			print(row)
		print("TextRank")
		for row in TextRankResult:
			print(row)
	 
	def searchKeyword(self,mode,q):
		q = q.lower()
		n_gram_query = q.split()
		lmtzr = WordNetLemmatizer()
		resultSet=[]
	 
		for item in n_gram_query:
			q = lmtzr.lemmatize(item) 
			resultSet+=self.getSearchRank(mode, q) 
			
		return self.computeRankResult(resultSet)
		
	def recommend(self, selected_document):
		CFfromTFLemma = self.getCF(2,selected_document) 
		resultTFLemma  = self.computeCFRanking(2,CFfromTFLemma, selected_document)
		 
		return resultTFLemma		
	

 
	def getCF(self,mode,selected_document):
		if 	mode ==0:
# 			result = self.graph.data("MATCH (n:Document)-[r1:TAG]->(k:Keyword)<-[r:TAG]-(p:Document)"
# 								"WHERE n.title ={keys}  "
# 								"RETURN p.title as title,k.word as keymatch, r.weight as weight  "
# 								"ORDER BY k.word", {"keys": selected_document})	
			result = self.graph.data("MATCH (n:Document)-[r1:TF_BASELINE]->(k:Keyword)<-[r:TF_BASELINE]-(p:Document)"
								"WHERE n.title ={keys}  "
								"RETURN p.title as title,k.word as keymatch, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})	
		
		elif mode ==1:
			result = self.graph.data("MATCH (n:Document)-[r1:TAG]->(k:Keyword)<-[r:TAG]-(p:Document)"
								"WHERE n.title ={keys}  "
								"RETURN p.title as title,k.word as keymatch, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})	
		elif mode ==2:
			result = self.graph.data("MATCH (n:Document)-[r1:TF_LEMMA]->(k:Keyword)<-[r:TF_LEMMA]-(p:Document)"
								"WHERE n.title ={keys}  "
								"RETURN p.title as title,k.word as keymatch, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})		
		elif mode ==3:
			result = self.graph.data("MATCH (n:Document)-[r1:RAKE]->(k:Keyword)<-[r:RAKE]-(p:Document)"
								"WHERE n.title ={keys}  "
								"RETURN p.title as title,k.word as keymatch, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})		
		
		else:
			result = self.graph.data("MATCH (n:Document)-[r1:TEXTRANK]->(k:Keyword)<-[r:TEXTRANK]-(p:Document)"
								"WHERE n.title ={keys}  "
								"RETURN p.title as title,k.word as keymatch, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})		
		return result		
		
	def getKeywordFromSelectedDocument(self,mode,selected_document):
		if mode ==0:
# 			result = self.graph.data("MATCH (n:Document)-[r:TAG]->(k:Keyword)"
# 								"WHERE n.title ={keys}  "
# 								"RETURN k.word as keywords, r.weight as weight  "
# 								"ORDER BY k.word", {"keys": selected_document})
			result = self.graph.data("MATCH (n:Document)-[r:TF_BASELINE]->(k:Keyword)"
								"WHERE n.title ={keys}  "
								"RETURN k.word as keywords, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})
		elif mode ==1:
			result = self.graph.data("MATCH (n:Document)-[r:TAG]->(k:Keyword)"
								"WHERE n.title ={keys}  "
								"RETURN k.word as keywords, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})
		
		elif mode ==2:
			result = self.graph.data("MATCH (n:Document)-[r:TF_LEMMA]->(k:Keyword)"
								"WHERE n.title ={keys}  "
								"RETURN k.word as keywords, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})
		
		elif mode ==3:
			result = self.graph.data("MATCH (n:Document)-[r:RAKE]->(k:Keyword)"
								"WHERE n.title ={keys}  "
								"RETURN k.word as keywords, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})
		
		else:
			result = self.graph.data("MATCH (n:Document)-[r:TEXTRANK]->(k:Keyword)"
								"WHERE n.title ={keys}  "
								"RETURN k.word as keywords, r.weight as weight  "
								"ORDER BY k.word", {"keys": selected_document})
		return result
			
	
	def getRecommendation(self,selected_document):
		
		CFfromTag = self.getCF(0,selected_document) 
		resultfromTag  = self.computeCFRanking(0,CFfromTag, selected_document)
				
		CFfromUserDefine = self.getCF(1,selected_document) 
		resultUserDefine  = self.computeCFRanking(1,CFfromUserDefine, selected_document)
		
		CFfromTFLemma = self.getCF(2,selected_document) 
		resultTFLemma  = self.computeCFRanking(2,CFfromTFLemma, selected_document)
		
		CFfromRAKE = self.getCF(3,selected_document) 
		resultRAKE  = self.computeCFRanking(3,CFfromRAKE, selected_document)
		
		CFfromTextRank = self.getCF(4,selected_document) 
		resultTextRank  = self.computeCFRanking(4,CFfromTextRank, selected_document)
		
 
# 		print("BaseLine")
# 		for item in resultfromTag:
# 			print(item) 
# 		print("TF_Stem")
# 		for item in resultTFBaseline:
# 			print(item)
# 		print("TF_Lemma")
# 		for item in resultTFLemma:
# 			print(item)
# 		print("RAKE")
# 		for item in resultRAKE:
# 			print(item)
# 		print("TextRank")
# 		for item in resultTextRank:
# 			print(item)
		
		#print("# of keyphrase used: ",keyphrase_item)	
		self.roc(resultfromTag, resultUserDefine, "User,")
		self.roc(resultfromTag, resultTFLemma, "TFLM,")
		self.roc(resultfromTag, resultRAKE, "RAKE,")
		self.roc(resultfromTag, resultTextRank, "TRnk,")
 
 		
 		 
	def getTitle(self,listofTuple):
		result = [a[0] for a in listofTuple]
		return result
	
	def Precision(self,basedlineTitle, testTitle):
	 	
		result = [x for x in testTitle if x in basedlineTitle]
		#print("Precision", result)
		if len(testTitle) ==0:
			return 0
		else:
			return len(result)/len(testTitle)

	def Recall(self,basedlineTitle,testTitle):
		#Recall (Sensitivity) = TPR = TP/P
		#P = total correct 
		P = basedlineTitle
		
		#TP correct result   
		TP = [x for x in basedlineTitle if x in testTitle]
		print("result, P, TP, FP, TN", len(testTitle), len(P) ,len(TP), end = " ")
		
		if len(basedlineTitle) == 0:
			return 0
		else:
			return len(TP)/len(P)
		
	def Fallout(self,basedlineTitle,testTitle):
		#Fallout = FPR= FP/N= FP/(FP+TN)
		# FP is answer set with False 
		#FP = incorrect answer
		FP = [x for x in testTitle if x not in basedlineTitle]
	
		print(len(FP), self.totalN, end = "  ---  ")
		return len(FP)/self.totalN
		
	
	def roc(self,basedline, test,name):
		
		
		basedlineTitle = self.getTitle(basedline)
		testTitle = self.getTitle(test)
		#precision = self.Precision(basedlineTitle, testTitle)
		TPR = self.Recall(basedlineTitle, testTitle)		
		FPR = self.Fallout(basedlineTitle, testTitle)
		
		TP = self.Precision(basedlineTitle, testTitle)
		if TP+TPR ==0:
			fmeasure = 0
		else:
			fmeasure = (2*TP*TPR)/(TP+TPR)
		
		print(name, end ="   ")	
		print("%.5f , %.5f, %.5f,  %.5f" % (FPR, TPR, TP, fmeasure))
 
		
	def FMeasure(self,basedline, test, name):
		print(name, end ="   ")	
		basedlineTitle = self.getTitle(basedline)
		testTitle = self.getTitle(test)
		precision = self.Precision(basedlineTitle, testTitle)
		recall = self.Recall(basedlineTitle, testTitle)
		
		
			
		print("P: ", precision, end ="   ")
		print("R: ", recall, end ="   ")
		if precision+recall ==0:
			fmeasure = 0
		else:
			fmeasure = (2*precision*recall)/(precision+recall)
		print("F: ", fmeasure)
		
		
		
		
	def spkitKeyword(self,keyworddict):
		keys = keyworddict.keys()
		ngramkey = {}
		for item in keys:
			if len(item) == 1:
				ngramkey[item] = item
			else:
				ngramlist = item.split()
				for i in ngramlist:
					ngramkey[i] = item
		return ngramkey
# 			
	def computeRecommendRanking(self, mode, resultlist,keywordlist):		
		 
		 	 
		resultset = set([d['title'] for d in resultlist])
		keywords = {a['keywords']:a['weight'] for a  in keywordlist}
		sorted_keywords = sorted(keywords.items(), key=operator.itemgetter(1),reverse=True)
		if mode ==0:
			keywords = dict(sorted_keywords)
		else:
			keywords = dict(sorted_keywords[0:self.keyphrase_item])
		
		if mode >2:
			splitkey = self.spkitKeyword(keywords)		 
		
		qresult = {}
		for item in resultset:
			score = 0
			itemlist = [d for d in resultlist if d['title'] == item]
			for i in itemlist:
				if mode <3:
					if keywords.get(i['keymatch']) is None:
						m = 0
					else:
						m = 1 * keywords.get(i['keymatch']) 
				else:
					if splitkey.get(i['keymatch']) is None:
						m=0
					else:
						m = 1* keywords.get(splitkey.get(i['keymatch']))/len(splitkey.get(i['keymatch']))
						
						#print(i['keymatch'],splitkey.get(i['keymatch']) )
				score += 1* i['weight']*m
			qresult[item] = score
		
		sorted_result = sorted(qresult.items(), key=lambda x: x[1], reverse=True)
		return sorted_result	
	
	
	def splitKeyphase(self,resultKeyphrase):
		splited = {}
		for i in resultKeyphrase:
			if len(i)>1:
				tmp = i.split()
				for j in tmp:
					if j in resultKeyphrase:
						splited[j] = resultKeyphrase[j] + resultKeyphrase[i]
					else:
						splited[j] = resultKeyphrase[i]
			else:
				if i in splited:
					splited[i] = resultKeyphrase[i] + splited[i]
		#splited = sorted(splited.items(), key=operator.itemgetter(1),reverse=True)
		return splited
		
	def cosine_similarity(self,v1,v2):
		#"compute cosine similarity of v1 to v2: (v1 dot v2)/{||v1||*||v2||)"
		sumxx, sumxy, sumyy = 0, 0, 0
		for i in range(len(v1)):
			x = v1[i]; y = v2[i]
			sumxx += x*x
			sumyy += y*y
			sumxy += x*y
		return sumxy/math.sqrt(sumxx*sumyy)		
		
	def computeCFRanking(self,mode, resultlist,selected_document):		 
		#resultlist = [{'keymatch': 'android', 'title': 'CMU Tower Defense', 'weight': 1},...]
		#goalkeylist =[{'keywords': 'android', 'weight': 1}, {'keywords': 'calories', 'weight': 1},...]
		
		goalkeylist = self.getKeywordFromSelectedDocument(mode, selected_document)
		TGK = {a['keywords']:a['weight'] for a in goalkeylist}
	

		TGK = sorted(TGK.items(), key=operator.itemgetter(1),reverse=True)
		 
		TargetKey,TargetSVM = zip(*TGK)
	
		
	
		
		resultset = set([d['title'] for d in resultlist])
		resultCos = []
		for result in resultset:
			tmp = {d['keymatch']:d['weight'] for d in resultlist if d['title'] == result}
			if mode >2:
				tmp = self.splitKeyphase(tmp)
 			 
			resultSVM = []
			for feature in TargetKey:
				if tmp.get(feature) is None:
					resultSVM.append(0)
				else:
					resultSVM.append(tmp.get(feature))
				
			
				
			resultCos.append(self.cosine_similarity(TargetSVM, resultSVM))
			
		dictionary = dict(zip(resultset, resultCos))
		sorted_RC = sorted(dictionary.items(), key=operator.itemgetter(1),reverse=True)
	 
		
		if mode == 0:
			return sorted_RC[0:self.rc_item]
		else:
			return sorted_RC[0:self.rc_item]
				

 			

##########################################################
################# Main first  ##################################
##########################################################



