from flask import Flask , render_template , request , url_for ,redirect,flash
from function import RecommendationSearch
#import pyorient
from py2neo import Graph
from py2neo import Node
from py2neo import Relationship

total = 66
rc_item =3
totalN = total - rc_item
keyphrase_item = 10

app = Flask(__name__)

@app.route('/')
def index():
	
	return render_template('index.html')

@app.route('/inputkey',methods=['GET','POST'])
def getKey():
	if request.method=="POST":
		keyword=request.form['keywords']

	Callobj=RecommendationSearch(object)
	Callobj.setdata(total, rc_item, keyphrase_item,totalN) 
	
	#searchresult =[(title,weight)]
	result = Callobj.searchKeyword(2, keyword)
	print(result)
	searchresult = [[a[0],a[1]] for a in result]
	return render_template('index.html',All_Detail=searchresult)


@app.route('/getdetail',methods=['GET','POST'])
def getDocDetail():
	title = ""
	if request.method=="GET":
		title = request.args.get('title', None)
	Callobj=RecommendationSearch(object)
	Callobj.setdata(total, rc_item, keyphrase_item,totalN) 	
	result = Callobj.getDocumentdetail(title)	
		
	print(title)
	recommend = Callobj.recommend(title)
	return render_template('result.html',detail=result,recommendation = recommend)

	#Callobj=RecommendationSearch(object)
	#Callobj.setdata(total, rc_item, keyphrase_item,totalN) 
if __name__=="__main__":
	app.run(debug=True)